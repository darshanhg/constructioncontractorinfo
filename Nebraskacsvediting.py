import csv
import os 
import re

'''directory = '/Users/darshangupta/Desktop/stackendscraping/ConstructionContractorInfo/scrapingNebraskaContractors'

# get all the filenames in the directory
filenames = os.listdir(directory)

# print only the filenames (not directories)
for filename in filenames:
    if os.path.isfile(os.path.join(directory, filename)):
        print(filename)'''

file_names = ['NebraskaOtherFoundationSidingAndBuildingContractors.csv',
'NebraskaPlumbingHeatingAndAirConditioningContractors.csv',
'NebraskaResidentialBuildingContractors.csv',
'NebraskaDrywallAndInsulationContractors.csv',
'NebraskaFinishCarpentryContractors.csv',
'NebraskaNewSingleFamilyHousingConstructors.csv',
'NebraskaRoofingContractors.csv',
'NebraskaNewMultiFamilyHousingConstructors.csv',
'NebraskaResidentialRemodelers.csv',
'NebraskaOtherBuildingEquipmentContractors.csv',
'NebraskaOilAndGasPipeLinesAndRelatedStructuresContractors.csv',
'NebraskaWaterAndSewerLinesAndRelatedStructuresContractors.csv',
'NebraskaNewHousingForSaleBuilders.csv',
'NebraskaPowerAndCommunicationLinesAndRelatedStructuresContractors.csv',
'NebraskaMasonryContractors.csv',
'NebraskaCommercialAndInstitutionalBuildingContractors.csv',
'NebraskaStructuredSteelAndPrecastContractors.csv',
'NebraskaSitePreparationContractors.csv',
'NebraskaFlooringContractors.csv',
'NebraskaTileAndTerrazzoContractors.csv',
'NebraskaSignInstallers.csv',
'NebraskaOtherHeavyAndCivilEngineeringConstructionContractors.csv',
'NebraskaHighwayStreetAndBridgeConstructionContractors.csv',
'NebraskaSidingContractors.csv',
'NebraskaPouredConcreteFoundationAndStructureContractors.csv',
'NebraskaGlazeAndGlazingContractors.csv',
'NebraskaPaintingAndWallCoveringContractors.csv',
'NebraskaElectricalAndOtherWireInstallationContractors.csv',
'NebraskaFramingContractors.csv',
'NebraskaNonResidentialBuildingConstruction.csv',
'NebraskaOtherBuildingFinishingContractors.csv']


import re
occupation = []
for i in range(0, len(file_names)):
    text = file_names[i]
    match = re.search(r"Nebraska(.*?)\.csv", text)

    if match:
        filename = match.group(1)
        filename = re.sub(r"(?<=[a-z])(?=[A-Z])", " ", filename) # Insert space between lowercase and uppercase letters
        filename = filename.replace("Contractors", "") # Remove "Contractors" from filename
        print(filename.strip())
        occupation.append(filename.strip())
#for i in occupation:
#    print(i)

for i in range(0, len(file_names)):
    file_name = file_names[i]
    occupation_name = occupation[i]
    # open the CSV file for reading
    with open(file_name, 'r') as infile:
        reader = csv.reader(infile)
        # get the header row
        header = next(reader)
        # add a new column to the header
        header.append('Contractor Type')

        # open a new file for writing
        with open('NebraskaContractors.csv', 'a', newline='') as outfile:
            writer = csv.writer(outfile)
            # write the updated header to the output file
            writer.writerow(header)
            
            # iterate over the rows in the input file
            for row in reader:
                # append a new value to each row
                row.append(occupation_name)
                # write the updated row to the output file
                writer.writerow(row)
