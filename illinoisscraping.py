import time
from selenium import webdriver
import pyautogui
from bs4 import BeautifulSoup
import re 
import csv
import requests

driver = webdriver.Chrome()
driver.get("https://online-dfpr.micropact.com/Lookup/LicenseLookup.aspx")
#URL = "https://licensing.ks.gov/verification_kbtp_213/SearchResults.aspx"
f = open('IllinoisStructuralEngineers.csv', 'w')
time.sleep(20)
writer = csv.writer(f)
first_row = ["Illinois Structural Engineer Name", "License Status", "Credential", "City/State", "Original Issue Date", "Current Expiration Date", "Ever Disciplined"]
writer.writerow(first_row)
for j in range(9):
    html = driver.page_source
    soup = str(BeautifulSoup(html, features='lxml'))
    arr = soup.split('HyperLinkDetail">Detail</a>')
    for i in range(1, len(arr)):
        text = arr[i]
        pattern = r"(?:</td><td>)(.*?)(?=<)"
        matches = re.findall(pattern, text)[:7]
        row = [matches[0], matches[1], matches[2], matches[3], matches[4], matches[5], matches[6]]
        writer.writerow(row)
    print(j)
    time.sleep(5) 