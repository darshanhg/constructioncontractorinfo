import csv

# Open the input CSV file for reading
with open("IndianaManufacturedHomeInstallers.csv", "r") as in_file:
    # Create a reader object
    reader = csv.reader(in_file)
    
    # Open the output CSV file for writing
    with open("IndianaContractPlumbers.csv", "w", newline="") as out_file:
        # Create a writer object
        writer = csv.writer(out_file)
        
        # Iterate through the rows in the input CSV file
        for row in reader:
            # Modify the third, fourth, and fifth entries
            row[2] = "Plumbing Commissions"
            row[3] = "Plumber Contractor"
            row[4] = "Active"
            
            writer.writerow(row)