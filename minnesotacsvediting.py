import csv
import os 

directory = '/Users/darshangupta/Desktop/stackendscraping/ConstructionContractorInfo/MinnesotaContractors'

# get all the filenames in the directory
filenames = os.listdir(directory)

# print only the filenames (not directories)
for filename in filenames:
    if os.path.isfile(os.path.join(directory, filename)):
        print(filename)

file_names = ['MinnesotaManufacturedStructures.csv',
'MinnesotaResidentialContractors.csv',
'MinnesotaElevatorContractors.csv',
'MinnesotaMechanicalBonds.csv',
'MinnesotaElectricians.csv',
'MinnesotaWaterConditioning.csv',
'MinnesotaHighPressurePiping.csv',
'MinnesotaPlumbing.csv']

occupation = ['Manufactured Structues',
'Residential',
'Elevator',
'Mechanical Bonds',
'Electricians',
'Water Conditioning',
'High Pressure Plumbing',
'Plumbing']


for i in range(0, len(file_names)):
    file_name = file_names[i]
    occupation_name = occupation[i]
    # open the CSV file for reading
    with open(file_name, 'r') as infile:
        reader = csv.reader(infile)
        # get the header row
        header = next(reader)
        # add a new column to the header
        header.append('Contractor Type')

        # open a new file for writing
        with open('MinnesotaContractors.csv', 'a', newline='') as outfile:
            writer = csv.writer(outfile)
            # write the updated header to the output file
            writer.writerow(header)
            
            # iterate over the rows in the input file
            for row in reader:
                # append a new value to each row
                row.append(occupation_name)
                # write the updated row to the output file
                writer.writerow(row)