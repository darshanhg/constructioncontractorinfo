from cgitb import text
import csv
import re 
from bs4 import BeautifulSoup
import requests
from selenium import webdriver
import time

URL = 'https://dol.nebraska.gov/conreg/Search'

f = open('NebraskaSitePreparationContractors.csv', 'w')
writer = csv.writer(f)

first_row = ["Contractor/Subcontractor Name", "Corporation Name", "Business Entity", "Address", "City", "State", "Zip", "Telephone", "Registration Number", "Registration Expiration", "Sales Tax Option", "Number of Employees", "Worker's Compensation Status"]
driver = webdriver.Chrome()

driver.get(URL)
#html = requests.get(URL).text

time.sleep(35)
content = driver.page_source
writer.writerow(first_row)
soup = str(BeautifulSoup(content, features="lxml"))

pattern = re.compile(r'<a class="btn btn-default btn-sm" href="(.*?)">')
matches = re.findall(pattern, soup)
for i in matches:
    #print(i)
    base = 'https://dol.nebraska.gov'
    fullURL = f"{base}{i}"
    html = requests.get(fullURL).text
    soup = str(BeautifulSoup(html, 'html.parser'))
    arr = str(html).split('<th>')
    row = []
    for i in range(1, len(arr)):
        text = arr[i]
        #desired_text = str(re.search("^(.*?)</font>", text).group(1))
        soup = BeautifulSoup(text, 'html.parser')
        td_tag = soup.find('td')
        if td_tag is not None:
            # Extract the text contents of the <td> tag
            result = td_tag.get_text()
            #print(str(result).strip())
            row.append(str(result).strip())
        else:
            print("No <td> tag found.")
    writer.writerow(row)
        